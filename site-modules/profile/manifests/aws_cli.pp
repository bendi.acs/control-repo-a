#
# profile::aws_cli
#

class profile::aws_cli {
  include profile::python

  class { '::aws_cli':
    services     => [  # the AWS services currently supported by moto
      'acm',
      'apigateway',
      'autoscaling',
      'lambda',
      'batch',
      'cloudformation',
      'cloudwatch',
      'cognitoidentity',
      'cognitoidp',
      'config',
      'datapipeline',
      'dynamodb',
      'dynamodbstreams',
      'ec2',
      'ecr',
      'ecs',
      'elb',
      'elbv2',
      'emr',
      'events',
      'glacier',
      'glue',
      'iam',
      'iot',
      'iotdata',
      'kinesis',
      'kms',
      'logs',
      'opsworks',
      'organizations',
      'polly',
      'rds',
      's3',
      's3api',
      'secretsmanager',
      'ses',
      'sns',
      'sqs',
      'ssm',
      'stepfunctions',
      'sts',
      'swf',
      'xray',
    ],
    aws_endpoint => 'http://dir.node.consul',  # TODO: figure out how to use a custom DNS name
  }

  Class['profile::python'] -> Class['::aws_cli']
}