#
# profile::moto_server
#

class profile::moto_server {
  include profile::python

  class { 'moto':
    http_port => 80,
  }

  Class['profile::python'] -> Class['moto']
}