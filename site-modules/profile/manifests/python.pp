#
# profile::python
#

class profile::python {
  class { 'python':
    version => 'system',
    pip     => 'present',
  }
}