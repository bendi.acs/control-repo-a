#
# profile::go
#

class profile::go {
  $go_workspace = lookup('go::go_workspace')
  $go_workspace_bin = lookup('go::go_workspace_bin')
  $go_workspace_pkg = lookup('go::go_workspace_pkg')
  $go_workspace_src = lookup('go::go_workspace_src')

  # Create directories for go workspace, they will be created in the right order, see https://www.puppetcookbook.com/posts/creating-a-directory-tree.html
  file { [$go_workspace, $go_workspace_bin, $go_workspace_pkg, $go_workspace_src]:
    ensure => directory,
    owner  => $facts['identity']['user'],
    group  => $facts['identity']['group'],
  }

  class { 'golang':
    version   => '1.12', # Mock EC2 metadata's dependencies fail to install using Go 1.13
    workspace => $go_workspace,
    require   => [
      File[$go_workspace],
      File[$go_workspace_bin],
      File[$go_workspace_pkg],
      File[$go_workspace_src],
    ],
  }

}