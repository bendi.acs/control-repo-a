#
# profile::ec2_metadata
#

class profile::ec2_metadata {
  include profile::go

  class { 'mock_ec2_metadata':
    user_data => 'This instance is running in OpenStack.',
    http_port => 80,
  }

  Class['profile::go'] -> Class['mock_ec2_metadata']
}