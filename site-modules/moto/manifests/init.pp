# @summary Installs and manages moto server.
#
# @example
#   include moto
class moto (
  Stdlib::Port $http_port,
  Stdlib::IP::Address::V4 $ip_address,
  Variant[Enum['default'], Integer[0, default]] $initial_no_auth_action_count,
  Boolean $service_manage,
) {
  contain moto::install
  contain moto::service
  Class['::moto::install'] ~> Class['::moto::service']
}
