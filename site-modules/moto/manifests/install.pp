# @summary Installs moto via pip.
class moto::install {
  python::pip { 'moto':
    ensure => '1.3.14.dev436',
    extras => ['server'],
  }
}
