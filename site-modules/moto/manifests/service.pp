# @summary Takes care of running moto server as a service.
class moto::service {

  if $moto::service_manage {
    $parameters = "-p${moto::http_port} -H ${moto::ip_address}"
    $service_template_config = {
      'initial_no_auth_action_count' => $moto::initial_no_auth_action_count,
      'parameters'                   => $parameters,
    }

    file { '/lib/systemd/system/moto.service':
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => epp('moto/moto.service.epp', $service_template_config),
    } ~> exec { 'moto-systemd-reload':
      command     => 'systemctl daemon-reload',
      path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
      refreshonly => true,
    }

    service { 'moto':
      ensure => running,
      enable => true,
    }
  }
}
