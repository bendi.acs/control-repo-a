#
# role::consul_server
#

class role::consul_server {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::server
}
