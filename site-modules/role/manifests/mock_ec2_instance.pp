#
# role::consul_server
#

class role::mock_ec2_instance {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::client
  include ::profile::ec2_metadata
  include ::profile::aws_cli
}
