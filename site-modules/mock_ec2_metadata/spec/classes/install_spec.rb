require 'spec_helper'

describe 'mock_ec2_metadata::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it {
        is_expected.to contain_package('git')
        is_expected.to contain_package('make')
        is_expected.to contain_vcsrepo('/opt/mock_ec2_metadata')
          .with(
            ensure: 'present',
            provider: 'git',
          )
        is_expected.to contain_exec('make deps')
          .with(
            refreshonly: true,
            cwd: '/opt/mock_ec2_metadata',
          )
        is_expected.to contain_exec('make')
          .with(
            refreshonly: true,
            cwd: '/opt/mock_ec2_metadata',
          )
      }
    end
  end
end
