require 'spec_helper'

describe 'mock_ec2_metadata::config' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it {
        is_expected.to contain_file('/etc/mock-ec2-metadata-config.json')
          .with(
            ensure: 'present',
          )
      }
    end
  end
end
