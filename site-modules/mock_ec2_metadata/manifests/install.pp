# @summary Installs mock_ec2_metadata and its dependencies.
#
# Downloads mock_ec2_metadata, git and make
# Installs the dependencies specified in the library's Makefile
# Buils the library as specified in the same Makefile
class mock_ec2_metadata::install {

  package { 'git':
    ensure => latest,
  }

  package { 'make':
    ensure => latest,
  }

  vcsrepo { $mock_ec2_metadata::install_directory:
    ensure   => present,
    provider => git,
    source   => 'git://github.com/andrasmaroy/mock-ec2-metadata.git',
    revision => 'tutorial-fixes',
    require  => [ Package['git'] ],
  }

  Exec {
    path        => '/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/go/bin/', # Include the path to the go executable
    environment => [
      'GOCACHE=/root/.cache/go-build',
      'GOPATH=/root/go',
    ]  # These environment variables need to be defined to successfully install the library's dependencies
  }

  # Install dependencies
  exec { 'make deps':
    cwd         => $mock_ec2_metadata::install_directory,
    require     => Package['make'],
    refreshonly => true,
    subscribe   => Vcsrepo[$mock_ec2_metadata::install_directory],
  }

  # Build the library
  exec { 'make':
    cwd         => $mock_ec2_metadata::install_directory,
    require     => Package['make'],
    refreshonly => true,
    subscribe   => Exec['make deps'],
  }
}
