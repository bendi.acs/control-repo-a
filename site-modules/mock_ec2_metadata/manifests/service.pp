# @summary Manages the mock_ec2_metadata service.
#
# Runs the platform- and architecture-specific executable in a service.
# Also creates the service file.
class mock_ec2_metadata::service {

  case $facts['os']['name'] {
    /^(Debian|Ubuntu|Scientific|RedHat|OracleLinux|CentOS)$/: {
      $platform = 'linux'
    }

    'Darwin': {
      $platform = 'darwin'
    }

    default: { notify { 'Which OS? WTF???': }
    }
  }

  if $mock_ec2_metadata::service_manage {
    $service_template_config = {
      'working_directory' => "${mock_ec2_metadata::install_directory}/bin",
      'executable_name'   => "mock-ec2-metadata_0.4.1_${platform}_${$facts['architecture']}",
    }

    file { '/lib/systemd/system/mock-ec2-metadata.service':
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => epp('mock_ec2_metadata/mock-ec2-metadata.service.epp', $service_template_config),
    } ~> exec { 'mock-ec2-metadata-systemd-reload':
      command     => 'systemctl daemon-reload',
      path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
      refreshonly => true,
    }

    service { 'mock-ec2-metadata':
      ensure => running,
      enable => true,
    }
  }
}
