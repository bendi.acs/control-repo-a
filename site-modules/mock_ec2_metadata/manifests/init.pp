# @summary Installs, configures and manages mock-ec2-metadata.
#
# @example
#   include mock_ec2_metadata
class mock_ec2_metadata (
  Stdlib::Absolutepath $install_directory,
  Stdlib::Port $http_port,
  Pattern[/^\.{0,2}\/([^\/\0]+\/*)*$/] $log_file, # Relative path
  Pattern[/^\.{0,2}\/([^\/\0]+\/*)*$/] $http_log_file, # Relative path
  Pattern[/^ami-[0-9a-f]+$/] $ami_id,
  Pattern[/^[0-9]+$/] $ami_launch_index,
  String $ami_manifest_path,
  String $availability_zone,
  String $hostname,
  String $instance_action,
  Pattern[/^i-[0-9a-f]+$/] $instance_id,
  String $instance_type,
  Stdlib::MAC $mac,
  String $profile,
  Pattern[/^r-[0-9a-f]+$/] $reservation_id,
  String $user,
  String $type,
  String $access_key_id,
  String $secret_access_key,
  String $token,
  String $expiration,
  String $last_updated,
  String $code,
  Array[Pattern[/^sg-[0-9a-f]+$/]] $security_groups,
  String $user_data,
  Array[Pattern[/^\/[0-9a-z\-.]+\/meta-data$/]] $metadata_prefixes,
  Array[Pattern[/^\/[0-9a-z\-.]+\/user-data$/]] $userdata_prefixes,
  Boolean $service_manage,
  Boolean $mock_ip_address,
) {
  contain mock_ec2_metadata::install
  contain mock_ec2_metadata::config
  contain mock_ec2_metadata::service
  Class['::mock_ec2_metadata::install'] -> Class['::mock_ec2_metadata::config']
  Class['::mock_ec2_metadata::config'] ~> Class['::mock_ec2_metadata::service']
}
