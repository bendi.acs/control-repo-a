# @summary Configures the mock_ec2_metadata library.
#
# Creates and fills mock-ec2-metadata-config.json.
# Optionally, binds an alias to the loopback interface,
# so traffic to the IP address 169.254.169.254 will be redirected to localhost.
class mock_ec2_metadata::config {

  $config_content = {
    'Server' => {
      'HTTPPort'      => $mock_ec2_metadata::http_port,
      'Log'           => $mock_ec2_metadata::log_file,
      'HTTPAccessLog' => $mock_ec2_metadata::http_log_file,
    },
    'MetadataValues' => {
      'ami-id'               => $mock_ec2_metadata::ami_id,
      'ami-launch-index'     => $mock_ec2_metadata::ami_launch_index,
      'ami-manifest-path'    => $mock_ec2_metadata::ami_manifest_path,
      'availability-zone'    => $mock_ec2_metadata::availability_zone,
      'hostname'             => $mock_ec2_metadata::hostname,
      'instance-action'      => $mock_ec2_metadata::instance_action,
      'instance-id'          => $mock_ec2_metadata::instance_id,
      'instance-type'        => $mock_ec2_metadata::instance_type,
      'mac'                  => $mock_ec2_metadata::mac,
      'profile'              => $mock_ec2_metadata::profile,
      'reservation-id'       => $mock_ec2_metadata::reservation_id,
      'user'                 => $mock_ec2_metadata::user,
      'security-credentials' => {
        'Type'            => $mock_ec2_metadata::type,
        'AccessKeyId'     => $mock_ec2_metadata::access_key_id,
        'SecretAccessKey' => $mock_ec2_metadata::secret_access_key,
        'Token'           => $mock_ec2_metadata::token,
        'Expiration'      => $mock_ec2_metadata::expiration,
        'LastUpdated'     => $mock_ec2_metadata::last_updated,
        'Code'            => $mock_ec2_metadata::code,
      },
      'security-groups'      => $mock_ec2_metadata::security_groups,
      'user-data'            => $mock_ec2_metadata::user_data,
    },
    'MetadataPrefixes' => $mock_ec2_metadata::metadata_prefixes,
    'UserdataPrefixes' => $mock_ec2_metadata::userdata_prefixes,
  }

  file { '/etc/mock-ec2-metadata-config.json':
    ensure  => 'present',
    content => to_json_pretty($config_content),
  }

  if $mock_ec2_metadata::mock_ip_address {
    network::interface { 'lo:1':
      ipaddress => '169.254.169.254',
      netmask   => '255.255.255.255',
    }
  }
}
