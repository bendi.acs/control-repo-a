# @summary Installs and configures AWS CLI and awscli-plugin-endpoint.
#
# @example
#   include aws_cli
class aws_cli (
  Stdlib::HTTPUrl $aws_endpoint,
  Array[String] $services,
) {
  contain aws_cli::install
  contain aws_cli::config
  Class['::aws_cli::install'] -> Class['::aws_cli::config']
}
