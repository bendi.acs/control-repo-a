# @summary Installs AWS CLI and awscli-plugin-endpoint via pip.
class aws_cli::install {
  python::pip { 'awscli':
    ensure => latest,
  }

  python::pip { 'awscli-plugin-endpoint':
    ensure => latest,
  }
}
