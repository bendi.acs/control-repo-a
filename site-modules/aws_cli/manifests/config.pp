# @summary Configures parameters for the AWS CLI (using the awscli-plugin-endpoint).
class aws_cli::config {
  $services = $aws_cli::services
  $aws_endpoint = $aws_cli::aws_endpoint

  $home_directory = $facts['identity']['user'] ? {
    'root'  => '/root',
    default => "/home/${facts['identity']['user']}",
  }

  file { "${home_directory}/.aws":
    ensure => directory,
    owner  => $facts['identity']['user'],
    group  => $facts['identity']['group'],
  }

  file { "${home_directory}/.aws/config":
    ensure  => present,
    owner   => $facts['identity']['user'],
    group   => $facts['identity']['group'],
    content => template('aws_cli/config.erb'),
    require => File["${home_directory}/.aws"],
  }
}
