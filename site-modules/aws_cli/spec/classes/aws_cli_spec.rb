require 'spec_helper'

describe 'aws_cli' do
  on_supported_os.each do |os, _|
    context "on #{os}" do
      let(:facts) do
        {
          identity: {
            user: 'root',
            group: 'root',
          },
        }
      end

      let(:params) do
        {
          aws_endpoint: 'http://moto.aws.local',
          services: ['ec2', 'dynamodb', 'iam'],
        }
      end

      it { is_expected.to compile }
    end
  end
end
