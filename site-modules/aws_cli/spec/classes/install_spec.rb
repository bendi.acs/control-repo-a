require 'spec_helper'

describe 'aws_cli::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it {
        is_expected.to contain_python__pip('awscli')
        is_expected.to contain_python__pip('awscli-plugin-endpoint')
      }
    end
  end
end
