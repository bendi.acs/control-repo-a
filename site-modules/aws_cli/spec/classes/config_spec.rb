require 'spec_helper'

describe 'aws_cli::config' do
  on_supported_os.each do |os, _|
    context "on #{os}" do
      let(:facts) do
        {
          identity: {
            user: 'root',
            group: 'root',
          },
        }
      end

      it { is_expected.to compile }

      it {
        is_expected.to contain_file('/root/.aws/config')
          .with(
            ensure: 'present',
            owner: 'root',
            group: 'root',
          )
      }
    end
  end
end
