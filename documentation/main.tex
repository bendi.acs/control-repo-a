\input{preamble.tex}

\title{AWS mock}
\author{Bendegúz Ács (520031)}

\begin{document}

\maketitle

\abstract{The cloud has been gaining popularity at an incredibly fast pace in recent years and this trend doesn't show any sign of slowing down \cite{Liu:19}. Also, it is expected that hybrid clouds will enjoy a significant increase in adoption in the near future. The most popular public cloud provider is AWS so there is a good chance that it is used in hybrid models as well \cite{Richter:19}. This paper presents a Puppet-based solution for mocking the AWS environment in a different, possibly private cloud (such as OpenStack). }

\thispagestyle{empty}

\clearpage
\pagenumbering{roman}
\setcounter{page}{1}
\tableofcontents

\clearpage
\pagenumbering{arabic}

\section{Introduction}

The main goal of this project is to assist in testing software solutions designed to run on AWS in a non-AWS cloud environment. This will be achieved by mocking the behavior of the AWS infrastructure. The implementation will consist of two main components:
\begin{itemize}
\item mock EC2 metadata service
\item moto (general mock framework for AWS)
\end{itemize}
The former will be responsible for making applications believe that they are running on an AWS server. In reality, however, these applications will be running on different servers, such as OpenStack (which is where this project will be deployed). Moto will be responsible for answering requests sent to AWS APIs. Whereas the mock EC2 metadata service should be deployed on (almost) all servers, moto is meant to be running on a single node.

Note that this solution will not actually provide a bridge between the API of AWS and another provider (OpenStack for example). Thus, for example, when you create a new EC2 instance in the environment set up by this project, no new virtual machine, such as an OpenStack server, will be created. Implementing this would require using non-infrastructure code (for example by extending the moto library), which would distract from the purpose of this project.

\section{Background technology}

\subsection{AWS}

AWS is the largest cloud provider today and it offers more than 150 services for a wide range of IT areas, such as compute, storage, databases and networking. Arguably, the most important service is Elastic Compute Cloud (abbreviated as EC2), which basically enables you to create virtual servers on demand. A closely related service is VPC, which deals with all the networking between the EC2 instances (the term AWS uses for its virtual servers). An AWS-managed version of Puppet Enterprise is also available in the OpsWorks service \cite{OpsWorks:17}. This can help you provision EC2 instances using Puppet without having to manually manage Puppet in your AWS infrastructure.

\subsection{AWS EC2 instance metadata}
\label{ec2-instance-metadata}

Every EC2 instance has a special, link-local IP address, available only from within the instance. This IP address is \lstinline{169.254.169.254} and this is used to host the metadata service for each instance. The metadata service provides general information about the instance itself, such as its unique identifier, its IP address and other networking details. Another important thing is that any application running on the instance can use the metadata service to acquire temporary AWS credentials that can be used to interact with AWS. For example, this can give an application permission to query data from DynamoDB. DynamoDB is a fully managed NoSQL database service in AWS.

\section{Survey of similar projects}

This solution uses a number of open source projects, each serving one particular purpose. This project combines them with the help of Puppet. Let's see what the role of each component project is.

\subsection{moto}

The most important component is arguably moto, an open source library that can mock out the AWS infrastructure \cite{Pulec:19}. The basic idea is that it can create and manipulate a variety of AWS resource in local memory, and using these in-memory objects, it can accurately answer requests that conform to the AWS API specification. The library currently partially covers the following services:
\begin{itemize}
    \item acm
    \item apigateway
    \item autoscaling
    \item lambda
    \item batch
    \item cloudformation
    \item cloudwatch
    \item cognitoidentity
    \item cognitoidp
    \item config
    \item datapipeline
    \item dynamodb
    \item dynamodbstreams
    \item ec2
    \item ecr
    \item ecs
    \item elb
    \item elbv2
    \item emr
    \item events
    \item glacier
    \item glue
    \item iam
    \item iot
    \item iotdata
    \item kinesis
    \item kms
    \item logs
    \item opsworks
    \item organizations
    \item polly
    \item rds
    \item s3
    \item s3api
    \item secretsmanager
    \item ses
    \item sns
    \item sqs
    \item ssm
    \item stepfunctions
    \item sts
    \item swf
    \item xray
\end{itemize}
Even though there are many more AWS services, moto does cover the most important ones.\\

To deploy moto, I created a \hyperref[moto-module]{Puppet module} and a \hyperref[profiles]{profile} for deploying moto on a single virtual machine which other virtual machines can use to communicate with.

\subsection{mock-ec2-metadata}

Mock-ec2-metadata is library meant to serve requests directed to the EC2 instance metadata service \cite{NYTimes:19}. This metadata service can be used to query a set of information about an EC2 instance running in the AWS cloud. For more information, see the \hyperref[ec2-instance-metadata]{AWS EC2 instance metadata section}.\\

The library provides a rich set of configuration options, basically any data returned by the mock service can be configured. For example, this includes the user data (arbitrary data defined by the user who starts the instance), the credentials the instance has and various networking details. The  \hyperref[mock-ec2-metadata-module]{Puppet module} I created to manage this library exposes all of these as parameters.

\subsection{AWS CLI + awscli-plugin-endpoint}

AWS CLI is a CLI wrapper for all of the AWS APIs. For example, if there is an EC2 operation \lstinline{DescribeInstances}, it can be invoked using the AWS CLI like this:
\begin{lstlisting}
aws cli describe-instances
\end{lstlisting}
If you would like to use an endpoint other the official AWS API's, you can specify it with the \lstinline{--endpoint-url} parameter. However, it can be tedious to add this parameter for every single command. Fortunately, there is a plugin called awscli-plugin-endpoint which allows you to define an endpoint to use for each service \cite{Li:19}. \\

I combined these two tools (AWS CLI and the aforementioned plugin) into a single \hyperref[aws-cli-module]{Puppet module}. This module has two parameters, one for the URL that will be used for AWS CLI command invocations and another for the AWS services for which the command will use the URL. Commands for all other services will call the default AWS API.

\section{Description of your work}

The solution has 5 main components:
\begin{itemize}
    \item Puppet module for moto
    \item Puppet module for mock-ec2-metadata
    \item Puppet module for AWS CLI and awscli-plugin-endpoint
    \item profile Puppet module to contain the tech/site-specific profiles
    \item role Puppet module to contain the business-specific server roles
\end{itemize}
All of these can be found in the GitLab repository \href{https://gitlab.com/bendi.acs/control-repo-a}{Control-Repo-A}. The configuration files of the test OpenStack environment are also hosted on GitLab, but in a \href{https://gitlab.com/bendi.acs/iac-heat-a}{different repository}. I also set up unit tests for each module. Unit testing, as well as linting and syntax checking is done automatically by the GitLab CI.

\subsection{Moto module}
\label{moto-module}

To install moto, the \lstinline[basicstyle=\color{oliveDrab}]{python::pip} custom resource is used from the puppet/python module \cite{Puppet:19}. The moto module does not install python itself, since python might be needed by other modules as well. The installation is done in the install.pp file:

\begin{minted}{puppet}
# @summary Installs moto via pip.
class moto::install {
  python::pip { 'moto':
    ensure => '1.3.14.dev436',
    extras => ['server'],
  }
}
\end{minted}

The main moto class provides an interface for users to specify the parameters and organizes the install and the service classes:

\begin{minted}{puppet}
# @summary Installs and manages moto server.
#
# @example
#   include moto
class moto (
  Stdlib::Port $http_port,
  Stdlib::IP::Address::V4 $ip_address,
  Variant[Enum['default'], Integer[0, default]] $initial_no_auth_action_count,
  Boolean $service_manage,
) {
  contain moto::install
  contain moto::service
  Class['::moto::install'] ~> Class['::moto::service']
}
\end{minted}

The default parameters of the module can be found in the module's common hiera file:

\begin{minted}{yaml}
---
moto::http_port: 5000
moto::ip_address: '0.0.0.0'
moto::initial_no_auth_action_count: default
moto::service_manage: true
\end{minted}

Finally, the service class is responsible for running the moto server as a service:

\begin{minted}{puppet}
# @summary Takes care of running moto server as a service.
class moto::service {

  if $moto::service_manage {
    $parameters = "-p${moto::http_port} -H ${moto::ip_address}"
    $service_template_config = {
      'initial_no_auth_action_count' => $moto::initial_no_auth_action_count,
      'parameters'                   => $parameters,
    }

    file { '/lib/systemd/system/moto.service':
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => epp('moto/moto.service.epp', $service_template_config),
    } ~> exec { 'moto-systemd-reload':
      command     => 'systemctl daemon-reload',
      path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
      refreshonly => true,
    }

    service { 'moto':
      ensure => running,
      enable => true,
    }
  }
}
\end{minted}

This class makes use of the following embedded Puppet template (EPP) file to generate the systemd service file:

\begin{minted}{erb}
<%- | $parameters,
      $initial_no_auth_action_count,
| -%>
# This file is managed by Puppet. Please do not make manual changes.
[Unit]
Description=Moto server for mocking AWS infrastructure
Wants=basic.target
After=basic.target network.target

[Service]
<% if $initial_no_auth_action_count != 'default' { -%>
Environment=INITIAL_NO_AUTH_ACTION_COUNT=<%= $initial_no_auth_action_count %>
<% } -%>
ExecStart=/usr/local/bin/moto_server <%= $parameters %>
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
\end{minted}

\subsection{AWS CLI module}
\label{aws-cli-module}

This module encompasses both AWS CLI and awscli-plugin-endpoint. The latter is a plugin that makes it easier to define custom endpoint URLs for the AWS operations. Both of them are installed via pip, so this module, just like the moto module requires a Python installation to be present on the machine, which should be done in a module. The installation of these pip packages is done by the following class in config.pp:

\begin{minted}{puppet}
# @summary Installs AWS CLI and awscli-plugin-endpoint via pip.
class aws_cli::install {
  python::pip { 'awscli':
    ensure => latest,
  }

  python::pip { 'awscli-plugin-endpoint':
    ensure => latest,
  }
}
\end{minted}

The module's main class, aws\_cli is responsible for exposing the necessary configurations parameters and organizing the other two classes, install and config:

\begin{minted}{puppet}
# @summary Installs and configures AWS CLI and awscli-plugin-endpoint.
#
# @example
#   include aws_cli
class aws_cli (
  Stdlib::HTTPUrl $aws_endpoint,
  Array[String] $services,
) {
  contain aws_cli::install
  contain aws_cli::config
  Class['::aws_cli::install'] -> Class['::aws_cli::config']
}
\end{minted}

There are no default parameters for this module, since this data is highly dependent on the specific use case.\\

The configuration is done by the config class. It creates a file at
\texttildelow/.aws/config which is the standard location of the AWS CLI's config file.

\begin{minted}{puppet}
# @summary Configures parameters for the AWS CLI (using the awscli-plugin-endpoint).
class aws_cli::config {
  $services = $aws_cli::services
  $aws_endpoint = $aws_cli::aws_endpoint

  $home_directory = $facts['identity']['user'] ? {
    'root'  => '/root',
    default => "/home/${facts['identity']['user']}",
  }

  file { "${home_directory}/.aws/":
    ensure => directory,
    owner  => $facts['identity']['user'],
    group  => $facts['identity']['group'],
  }

  file { "${home_directory}/.aws/config":
    ensure  => present,
    owner   => $facts['identity']['user'],
    group   => $facts['identity']['group'],
    content => template('aws_cli/config.erb'),
    require => File["${home_directory}/.aws/"],
  }
}
\end{minted}

The config file is constructed from an embedded Ruby template (ERB). The reason for using an ERB instead of an EPP is that the EPP does not support iterations, which would be needed for easy handling of the array of services.

\begin{minted}{erb}
# This file is managed by Puppet. Please do not make manual changes.
[default]
output = json
region = eu-central-1
<% @services.each do |service| -%>
<%= service %> =
    endpoint_url = <%= @aws_endpoint %>
<% end -%>
[plugins]
endpoint = awscli_plugin_endpoint
\end{minted}

\subsection{Mock EC2 metadata module}
\label{mock-ec2-metadata-module}

This Puppet module installs, configures and creates a service for the mock\_ec2\_metadata library. Since the library is written in Go, the module requires a go installation to be present, but does not take care of installing it for itself. Instead, this should be done in a profile. The reason is that it might be needed by other modules on the same virtual machine, similarly to the Python installation. The install.pp takes care of downloading the repository (using the
puppetlabs/vcsrepo Puppet module \cite{PuppetLabs:19}), as well as installing the library's dependencies and building the Go executable (using make for both of these steps).

\begin{minted}[breaklines]{puppet}
# @summary Installs mock_ec2_metadata and its dependencies.
#
# Downloads mock_ec2_metadata, git and make
# Installs the dependencies specified in the library's Makefile
# Buils the library as specified in the same Makefile
class mock_ec2_metadata::install {

  package { 'git':
    ensure => latest,
  }

  package { 'make':
    ensure => latest,
  }

  vcsrepo { $mock_ec2_metadata::install_directory:
    ensure   => present,
    provider => git,
    source   => 'git://github.com/andrasmaroy/mock-ec2-metadata.git',
    revision => 'tutorial-fixes',
    require  => [ Package['git'] ],
  }

  Exec {
    path        => '/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/go/bin/', # Include the path to the go executable
    environment => [
      'GOCACHE=/root/.cache/go-build',
      'GOPATH=/root/go',
    ]  # These environment variables need to be defined to successfully install the library's dependencies
  }

  # Install dependencies
  exec { 'make deps':
    cwd         => $mock_ec2_metadata::install_directory,
    require     => Package['make'],
    refreshonly => true,
    subscribe   => Vcsrepo[$mock_ec2_metadata::install_directory],
  }

  # Build the library
  exec { 'make':
    cwd         => $mock_ec2_metadata::install_directory,
    require     => Package['make'],
    refreshonly => true,
    subscribe   => Exec['make deps'],
  }
}

\end{minted}

The library has a lot of configuration options, and all of these are also exposed by the Puppet module. The config class ensures that the configuration file is correctly created for the library to use.

\begin{minted}[breaklines]{puppet}
# @summary Configures the mock_ec2_metadata library.
#
# Creates and fills mock-ec2-metadata-config.json.
# Optionally, binds an alias to the loopback interface,
# so traffic to the IP address 169.254.169.254 will be redirected to localhost.
class mock_ec2_metadata::config {

  $config_content = {
    'Server' => {
      'HTTPPort'      => $mock_ec2_metadata::http_port,
      'Log'           => $mock_ec2_metadata::log_file,
      'HTTPAccessLog' => $mock_ec2_metadata::http_log_file,
    },
    'MetadataValues' => {
      'ami-id'               => $mock_ec2_metadata::ami_id,
      'ami-launch-index'     => $mock_ec2_metadata::ami_launch_index,
      'ami-manifest-path'    => $mock_ec2_metadata::ami_manifest_path,
      'availability-zone'    => $mock_ec2_metadata::availability_zone,
      'hostname'             => $mock_ec2_metadata::hostname,
      'instance-action'      => $mock_ec2_metadata::instance_action,
      'instance-id'          => $mock_ec2_metadata::instance_id,
      'instance-type'        => $mock_ec2_metadata::instance_type,
      'mac'                  => $mock_ec2_metadata::mac,
      'profile'              => $mock_ec2_metadata::profile,
      'reservation-id'       => $mock_ec2_metadata::reservation_id,
      'user'                 => $mock_ec2_metadata::user,
      'security-credentials' => {
        'Type'            => $mock_ec2_metadata::type,
        'AccessKeyId'     => $mock_ec2_metadata::access_key_id,
        'SecretAccessKey' => $mock_ec2_metadata::secret_access_key,
        'Token'           => $mock_ec2_metadata::token,
        'Expiration'      => $mock_ec2_metadata::expiration,
        'LastUpdated'     => $mock_ec2_metadata::last_updated,
        'Code'            => $mock_ec2_metadata::code,
      },
      'security-groups'      => $mock_ec2_metadata::security_groups,
      'user-data'            => $mock_ec2_metadata::user_data,
    },
    'MetadataPrefixes' => $mock_ec2_metadata::metadata_prefixes,
    'UserdataPrefixes' => $mock_ec2_metadata::userdata_prefixes,
  }

  file { '/etc/mock-ec2-metadata-config.json':
    ensure  => 'present',
    content => to_json_pretty($config_content),
  }

  if $mock_ec2_metadata::mock_ip_address {
    network::interface { 'lo:1':
      ipaddress => '169.254.169.254',
      netmask   => '255.255.255.255',
    }
  }
}

\end{minted}

The configuration options can be provided to the module's main class, mock\_ec2\_metadata. This class also serves as a container for the other classes declared in this module and it also sets up the dependencies and ordering between them.

\begin{minted}{puppet}
# @summary Installs, configures and manages mock-ec2-metadata.
#
# @example
#   include mock_ec2_metadata
class mock_ec2_metadata (
  Stdlib::Absolutepath $install_directory,
  Stdlib::Port $http_port,
  Pattern[/^\.{0,2}\/([^\/\0]+\/*)*$/] $log_file, # Relative path
  Pattern[/^\.{0,2}\/([^\/\0]+\/*)*$/] $http_log_file, # Relative path
  Pattern[/^ami-[0-9a-f]+$/] $ami_id,
  Pattern[/^[0-9]+$/] $ami_launch_index,
  String $ami_manifest_path,
  String $availability_zone,
  String $hostname,
  String $instance_action,
  Pattern[/^i-[0-9a-f]+$/] $instance_id,
  String $instance_type,
  Stdlib::MAC $mac,
  String $profile,
  Pattern[/^r-[0-9a-f]+$/] $reservation_id,
  String $user,
  String $type,
  String $access_key_id,
  String $secret_access_key,
  String $token,
  String $expiration,
  String $last_updated,
  String $code,
  Array[Pattern[/^sg-[0-9a-f]+$/]] $security_groups,
  String $user_data,
  Array[Pattern[/^\/[0-9a-z\-.]+\/meta-data$/]] $metadata_prefixes,
  Array[Pattern[/^\/[0-9a-z\-.]+\/user-data$/]] $userdata_prefixes,
  Boolean $service_manage,
  Boolean $mock_ip_address,
) {
  contain mock_ec2_metadata::install
  contain mock_ec2_metadata::config
  contain mock_ec2_metadata::service
  Class['::mock_ec2_metadata::install'] -> Class['::mock_ec2_metadata::config']
  Class['::mock_ec2_metadata::config'] ~> Class['::mock_ec2_metadata::service']
}
\end{minted}

Finally, there is another class in this module, service, which manages the service for the mock\_ec2\_metadata executable.

\begin{minted}[breaklines]{puppet}
# @summary Manages the mock_ec2_metadata service.
#
# Runs the platform- and architecture-specific executable in a service.
# Also creates the service file.
class mock_ec2_metadata::service {

  case $facts['os']['name'] {
    /^(Debian|Ubuntu|Scientific|RedHat|OracleLinux|CentOS)$/: {
      $platform = 'linux'
    }

    'Darwin': {
      $platform = 'darwin'
    }

    default: { notify { 'Which OS? WTF???': }
    }
  }

  if $mock_ec2_metadata::service_manage {
    $service_template_config = {
      'working_directory' => "${mock_ec2_metadata::install_directory}/bin",
      'executable_name'   => "mock-ec2-metadata_0.4.1_${platform}_${$facts['architecture']}",
    }

    file { '/lib/systemd/system/mock-ec2-metadata.service':
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => epp('mock_ec2_metadata/mock-ec2-metadata.service.epp', $service_template_config),
    } ~> exec { 'mock-ec2-metadata-systemd-reload':
      command     => 'systemctl daemon-reload',
      path        => [ '/usr/bin', '/bin', '/usr/sbin' ],
      refreshonly => true,
    }

    service { 'mock-ec2-metadata':
      ensure => running,
      enable => true,
    }
  }
}
\end{minted}

To start the systemd service, the service class creates the necessary service file from the following embedded Puppet template (EBB):

\begin{minted}{erb}
<%- | $working_directory,
      $executable_name,
| -%>
# This file is managed by Puppet. Please do not make manual changes.
[Unit]
Description=Mock EC2 metadata service
Wants=basic.target
After=basic.target network.target

[Service]
WorkingDirectory=<%= $working_directory %>
ExecStart=<%= $working_directory %>/<%= $executable_name %>
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
\end{minted}

\subsection{Profiles}
\label{profiles}

Apart from the profiles that were already created (client + server for dns and consul, base\_linux), I added 5 new ones, one for the each new module, one for Python and one for Go.\\

The profiles for the modules are relatively straightforward, there is only one thing worthy of note: as mentioned previously, instead of directly setting up an environment for a programming language (Go or Python), the modules leave this task to a profile. Therefore, to make sure that the profiles for the modules are self-contained, they \lstinline[basicstyle=\color{editorGreen}, keywordstyle=\bfseries, morekeywords={include}]{include} a profile for the required programming language environment. The profiles for the modules can be found in \hyperref[appendix-a]{appendix A}.\\

The module responsible for setting up the Python environment is also straightforward, it declares a single class from the puppet/python module \cite{Puppet:19}.

\begin{minted}{puppet}
#
# profile::python
#

class profile::python {
  class { 'python':
    version => 'system',
    pip     => 'present',
  }
}
\end{minted}

The go profile also uses an external module, dcoxall/golang \cite{Coxall:16}. Apart from declaring a class from this module, it creates the go workspace directory tree.

\begin{minted}[breaklines]{puppet}
#
# profile::go
#

class profile::go {
  $go_workspace = lookup('go::go_workspace')
  $go_workspace_bin = lookup('go::go_workspace_bin')
  $go_workspace_pkg = lookup('go::go_workspace_pkg')
  $go_workspace_src = lookup('go::go_workspace_src')

  # Create directories for go workspace, they will be created in the right order, see https://www.puppetcookbook.com/posts/creating-a-directory-tree.html
  file { [$go_workspace, $go_workspace_bin, $go_workspace_pkg, $go_workspace_src]:
    ensure => directory,
    owner  => $facts['identity']['user'],
    group  => $facts['identity']['group'],
  }

  class { 'golang':
    version   => '1.12', # Mock EC2 metadata's dependencies fail to install using Go 1.13
    workspace => $go_workspace,
    require   => [
      File[$go_workspace],
      File[$go_workspace_bin],
      File[$go_workspace_pkg],
      File[$go_workspace_src],
    ],
  }

}
\end{minted}

\subsection{Roles}
\label{roles}

Following the Puppet best practice of using profiles and roles \cite{Larizza:16}, I also created roles for the business-specific server roles. Each of them \lstinline[basicstyle=\color{editorGreen}, keywordstyle=\bfseries, morekeywords={include}]{include}s some profiles that are needed to fulfill the given role. Their implementation is shown in \hyperref[appendix-b]{appendix B}.

\subsection{CI, unit testing}
\label{ci}

Since the project was developed on GitLab, it was a great opportunity to enable GitLab's CI (Continuous Integration) solution for it. The modules were created using PDK (Puppet Development Kit ), which automatically created the GitLab CI configuration files. However, these modules were meant to be placed in a separate repo of their own, which was not an ideal organization for this project. Therefore, I had to create a top-level configuration file, which included the module-level ones and performed the validation of the profiles and roles. This was not enough to get it working, I had to modify the configuration files created by PDK too, for example, to avoid conflicting task names. Apart from unit tests, syntax checking and linting are also done by the CI pipeline. The CI configuration files can be found in \hyperref[appendix-c]{appendix C}.\\

After the CI pipeline was set up and working, I added a few more unit tests to the modules too. Here are some examples unit tests that I created based on the basic test files originally added by PDK.

\begin{minted}{ruby}
require 'spec_helper'

describe 'mock_ec2_metadata::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it {
        is_expected.to contain_package('git')
        is_expected.to contain_package('make')
        is_expected.to contain_vcsrepo('/opt/mock_ec2_metadata')
          .with(
            ensure: 'present',
            provider: 'git',
          )
        is_expected.to contain_exec('make deps')
          .with(
            refreshonly: true,
            cwd: '/opt/mock_ec2_metadata',
          )
        is_expected.to contain_exec('make')
          .with(
            refreshonly: true,
            cwd: '/opt/mock_ec2_metadata',
          )
      }
    end
  end
end
\end{minted}

\begin{minted}{ruby}
require 'spec_helper'

describe 'aws_cli::install' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it {
        is_expected.to contain_python__pip('awscli')
        is_expected.to contain_python__pip('awscli-plugin-endpoint')
      }
    end
  end
end

\end{minted}

\begin{minted}{ruby}
require 'spec_helper'

describe 'moto::service' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }

      it {
        is_expected.to contain_service('moto')
          .with(
            ensure: 'running',
            enable: true,
          )
      }
    end
  end
end
\end{minted}

\section{Results and discussion}

\subsection{Results}

The project has achieved the objectives I set out to accomplish. Better yet, I even had time to add some unit tests and enable CI, both of which were not not strictly a planned goal of the project.\\

My initial plan included using moto and the mock EC2 metadata service to create an AWS-like infrastructure in the test OpenStack environment. Apart from these modules, I also decided to include AWS CLI. The reason for this is that I wanted to demonstrate and test that the setup is working without having to specify the endpoint URL of the moto server for every AWS command. This way, the commands can be the exact same command you would need to execute against the real AWS API. An even better way to achieve the same effects would be to somehow redirect the real AWS URLs to the moto server, but this would have involved having to install an SSL certificate (since the AWS clients use HTTPS by default), and figure out how to set up the local DNS resolver to enable this redirection. This would have been a very difficult task, so I decided to take the shortcut of overwriting the endpoint URL with the help of the awscli-plugin endpoint \cite{Li:19}.\\

At the end of the project, I run a quick test on my OpenStack deployment to see if everything works as expected. The following screenshot proves this result:

\begin{landscape}

\newpage
\begin{textblock*}{297mm}(0mm,0mm)
	\includegraphics[width=1\paperheight,angle=90]{aws-mock-test.PNG}
	\thispagestyle{empty}
\end{textblock*}
\null\newpage
\end{landscape}

As you can see, I created some instances on one Ubuntu server (lin2) in OpenStack and queried them on another (lin0). You can tell its the same instance by the instance ID. How do we know it was not executed against the real AWS API? I started 10 x1e.32xlarge instances, each costing around \$40 per hour. Even if I would just let them run for one minute, it would still cost me more than \$60, which of course, I would prefer not to pay just for testing some instances.\\

I also tested the mock metadata service:\\

\includegraphics{aws-mock-test-metadata.PNG}

\subsection{Encountered problems}

The following lists summarizes the major problems I've been faced with during the development of this project:

\begin{itemize}
    \item The setup that I started working with initially contained a Windows server as well. Unfortunately, I had to remove this, since I found out that there are many things that simply cannot be accomplished on Windows with Puppet (well, without having to write your custom implementation of course, but this would be massively out of the scope of this project). For example, the go library required the \lstinline{make} command to work, which is generally not available on Windows, and the Puppet go installation does not support Windows at all either \cite{Coxall:16}.
    \item At first, I was under the wrong assumption that the \lstinline[basicstyle=\color{editorGreen}, keywordstyle=\bfseries, morekeywords={service}]{service} resource type automatically creates a new service instead of only managing an existing one. However, it does not, and when I tried to pass the path to an executable as a parameter to the \lstinline[basicstyle=\color{editorGreen}, keywordstyle=\bfseries, morekeywords={service}]{service} resource and then run \lstinline{puppet agent -t}, the command was stuck, it never returned. I managed to resolve this problem by creating a new systemd service file in the appropriate directory, and then simply passing its name to the Puppet resource. This has to be done for both the \hyperref[moto-module]{moto} and the \hyperref[mock-ec2-instance-module]{mock-ec2-metadata} modules.
    \item During the setup of the \hyperref[ci]{GitLab CI}, I encountered several problems. The most important one was probably that I wanted to use puppet commands (such as \lstinline{puppet parser validate} and \lstinline{puppet-lint}) to validate the syntax and lint the modules, but I didn't know how to do that. The PDK-created modules did not contain examples for that, since they use a Ruby-based linter, so I couldn't use these modules as a source. In the end, I managed to install Puppet using a custom script, but then I run into errors from the puppet executable. Looking more deeply into how this should be done, I found out that the CI runners on GitLab can run a custom Docker image. Then I realized that this is probably a much easier, faster and more stable solution than to manually install Puppet every time, so I switched to this approach.
    \item Another related problem was that the first Docker image I found for Puppet (puppet/puppetserver) did not have puppet-lint installed on it, so it still had to be manually installed. And for some reason, the \lstinline{puppet-lint} command run without error, but did not find any linting errors. When I realized this, I searched a bit more, found another Puppet Docker image (puppet/puppet-dev-tools), and then switched to this. As soon as the new image was used, the \lstinline{puppet-lint} started working correctly, giving me a number of linting errors that had remained undiscovered previously.
    \item Just the day after I enabled \hyperref[ci]{GitLab CI} to run unit tests and validate my code, there was a large CI outage at GitLab affecting a lot of users \cite{GitLab:19}. Due to this incident, I decided to focus on writing the documentation instead of working on unit tests for that day.
\end{itemize}

\section{Security aspects}

This solution does not secure communication between the mock EC2 instances and the server hosting the moto service. This is a vulnerability and can be exploited by a man-in-the-middle attack. One simple mitigation could be to prevent any internal traffic from leaving your private cloud where this solution is deployed, and making sure that the servers are secure enough so they will not be taken over by a hacker.\\

One natural improvement for this project is to install an SSL certificate on the moto server, and then program the clients to use HTTPS instead of HTTP.\\

Another different security aspect is that moto has the ability to do basic authentication and authorization for incoming requests. This enables the testing of some security-related parts of an AWS infrastructure using this project.

\section{Conclusions}

I definitely consider this project to be successful and I would recommend using it to anyone who has access to a hybrid cloud and would like to test their AWS infrastructure in the private part of their cloud.

\afterpage{\null\newpage}

\bibliographystyle{acmdoi}
\bibliography{template}

\clearpage
\appendix

\section{Profiles for modules}
\label{appendix-a}

\begin{minted}[breaklines]{puppet}
#
# profile::aws_cli
#

class profile::aws_cli {
  include profile::python

  class { '::aws_cli':
    services     => [  # the AWS services currently supported by moto
      'acm',
      'apigateway',
      'autoscaling',
      'lambda',
      'batch',
      'cloudformation',
      'cloudwatch',
      'cognitoidentity',
      'cognitoidp',
      'config',
      'datapipeline',
      'dynamodb',
      'dynamodbstreams',
      'ec2',
      'ecr',
      'ecs',
      'elb',
      'elbv2',
      'emr',
      'events',
      'glacier',
      'glue',
      'iam',
      'iot',
      'iotdata',
      'kinesis',
      'kms',
      'logs',
      'opsworks',
      'organizations',
      'polly',
      'rds',
      's3',
      's3api',
      'secretsmanager',
      'ses',
      'sns',
      'sqs',
      'ssm',
      'stepfunctions',
      'sts',
      'swf',
      'xray',
    ],
    aws_endpoint => 'http://dir.node.consul',  # TODO: figure out how to use a custom DNS name
  }

  Class['profile::python'] -> Class['::aws_cli']
}


#
# profile::ec2_metadata
#

class profile::ec2_metadata {
  include profile::go

  class { 'mock_ec2_metadata':
    user_data => 'This instance is running in OpenStack.',
    http_port => 80,
  }

  Class['profile::go'] -> Class['mock_ec2_metadata']
}


#
# profile::moto_server
#

class profile::moto_server {
  include profile::python

  class { 'moto':
    http_port => 80,
  }

  Class['profile::python'] -> Class['moto']
}
\end{minted}

\section{Roles}
\label{appendix-b}

\begin{minted}{puppet}
#
# role::consul_server
#

class role::consul_server {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::server
}


#
# role::directory_server
#

class role::directory_server {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::dns::server
  include ::profile::consul::server
  include ::profile::moto_server
}


#
# role::consul_server
#

class role::mock_ec2_instance {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::client
  include ::profile::ec2_metadata
  include ::profile::aws_cli
}
\end{minted}

\section{GitLab CI configuration files}
\label{appendix-c}

\subsection{Top-level .gitlab-ci.yml}
\begin{minted}[breaklines]{yaml}
---
stages:
  - syntax
  - lint
  - unit

default:
  image: "puppet/puppet-dev-tools"  # For some reason, puppet cannot be used when installed as a gem.
  cache:
    paths:
      - vendor/bundle

.role_base:
  before_script:
    - pushd site-modules/role
  only:
    changes:
      - site-modules/role/**/*

.profile_base:
  before_script:
    - pushd site-modules/profile
  only:
    changes:
      - site-modules/profile/**/*

role syntax check:
  extends: .role_base
  stage: syntax
  script:
    - puppet parser validate .

profile syntax check:
  extends: .profile_base
  stage: syntax
  script:
    - puppet parser validate .

role lint:
  extends: .role_base
  stage: lint
  script:
    - puppet-lint . --error-level all --fail-on-warnings

profile lint:
  extends: .profile_base
  stage: lint
  script:
    - puppet-lint . --error-level all --fail-on-warnings

include:
  - local: site-modules/aws_cli/.gitlab-ci.yml
  - local: site-modules/mock_ec2_metadata/.gitlab-ci.yml
  - local: site-modules/moto/.gitlab-ci.yml

\end{minted}

\subsection{Module-level .gitlab-ci.yml (for aws\_cli, others are very similar)}
\begin{minted}[breaklines]{yaml}
---
.aws_cli_base:
  before_script:
    - pushd site-modules/aws_cli
  only:
    changes:
      - site-modules/aws_cli/**/*

.aws_cli_base_ruby:
  extends: .aws_cli_base
  image: ruby:2.5.3
  before_script:
    - pushd site-modules/aws_cli
    - bundle -v
    - rm Gemfile.lock || true
    - gem update --system $RUBYGEMS_VERSION
    - gem --version
    - bundle -v
    - bundle install --without system_tests --path vendor/bundle --jobs $(nproc)

aws_cli syntax check:
  extends: .aws_cli_base
  stage: syntax
  script:
    - puppet parser validate .

aws_cli lint Puppet:
  extends: .aws_cli_base
  stage: lint
  script:
    - puppet-lint . --error-level all --fail-on-warnings

aws_cli lint Ruby Puppet ~> 6:
  extends: .aws_cli_base_ruby
  stage: lint
  script:
    - bundle exec rake syntax lint metadata_lint check:symlinks check:git_ignore check:dot_underscore check:test_file rubocop
  variables:
    PUPPET_GEM_VERSION: '~> 6'

aws_cli unit tests Puppet ~> 6:
  extends: .aws_cli_base_ruby
  stage: unit
  script:
    - bundle exec rake parallel_spec
  variables:
    PUPPET_GEM_VERSION: '~> 6'

aws_cli unit tests Puppet ~> 5:
  extends: .aws_cli_base_ruby
  stage: unit
  image: ruby:2.4.5
  script:
    - bundle exec rake parallel_spec
  variables:
    PUPPET_GEM_VERSION: '~> 5'

\end{minted}

\end{document}
