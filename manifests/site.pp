node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}

node /lin\d?.node.consul/ {
  include ::role::mock_ec2_instance
}
node /(manager|mon).node.consul/ {
  include ::role::consul_server
}
node 'dir.node.consul' {
  include ::role::directory_server
}

